# ##################################################
# Project settings
#+TODO: TODO | EXAMPLE DOC SPACER
# ##################################################

* Interactive CV :interactive_cv:
[[https://en.wikipedia.org/wiki/Static_web_page][Static site]] with [[https://en.wikipedia.org/wiki/Reactive_programming][reactive]] [[add-content#Declare components][components]] and an [[workflow][interactive workflow]].

Mirrored at [[gitlab:kimok/interactive-cv]].

** Deployments
- [[https://kimo.life][kimo.life]] :: Kimo's portfolio.
- [[https://alixanneshaw.com][Alix Anne Shaw]] :: Alix's portfolio.
- [[https://demo.kimo.life][demo]] :: this document.

** Get Started :get_started:
First, install the dependencies.

Interactive-cv depends on [[clojure.org][clojure]] and [[npmjs.com][npm]].

*** Terminal
For a live development build, run:
#+begin_src bash
git clone https://gitlab.com/kimok/interactive-cv
cd interactive-cv
npm install
npx shadow-cljs watch app
#+end_src
Then, visit [[http://localhost:8081][localhost:8081]]

*** Kimacs
My emacs distribution [[https://gitlab.com/kimok/kimacs][kimacs]] has shortcuts for interactive-cv:

- ~<escape> p I~ :: install interactive-cv
- ~<escape> p c~ :: open the content file
- ~<escape> p s~ :: start the local sever
- ~<escape> p b~ :: open the site in a browser

** Workflow :workflow:

Interactive-cv renders an org-mode file as you write it with a [[https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop][REPL]].

It publishes content to the web with [[https://docs.gitlab.com/ee/ci/][Gitlab CI/CD]].


*** Author your content :author:
Edit your ~content.org~ file to add content to the site.

It should exist at ~src/content/<your_username>/content.org~.

Interactive-cv renders a new site each time you save ~content.org~.

No need to refresh the page!

**** SPACER 
The content of this page is in  [[https://gitlab.com/kimok/interactive-cv/-/tree/demo/readme.org][src/content/demo/content.org]].

**** Elements
Interactive-cv renders elements of [[https://orgmode.org/orgguide.html][org-mode]] syntax. For instance:

#+begin_src
 ***** Heading
*bold* /italic/ _underline_ =verbatim= ~code~ +strike+

[[https://example.org][web link]]

[[example][internal link]]

- list 
  + [ ] checklist
  + [ ] checklist
     1) ordered list
     2) ordered list
        
| table | 1 | 2 |
| table | 3 | 4 |

#+begin_src                                                   .
Code block
#+end_src                                                     .

#+begin_quote :author kimok                                   .
Quote block
#+end_quote                                                   .

-----
#+end_src
-----

***** Heading
*bold* /italic/ _underline_ =verbatim= ~code~ +strike+

[[https://example.org][web link]]

[[example][internal link]]

- list 
  + [ ] checklist
  + [ ] checklist
    1) ordered list
    2) ordered list
        
| table | 1 | 2 |
| table | 3 | 4 |

#+begin_src
Code block
#+end_src

#+begin_quote :author kimok
Quote block
#+end_quote

#+begin_citation :author kimok
Special block
#+end_citation


**** Components
A [[https://orgmode.org/orgguide.html#Headlines][heading]] with a [[https://orgmode.org/org.html#TODO-Basics][keyword]] declares a [[https://en.wikipedia.org/wiki/Component-based_software_engineering][component]].

For instance:

#+begin_src
 ** EXAMPLE Hello World!
 this is an example component.
#+end_src
***** EXAMPLE Hello World!
This is an example component.

***** Self-documentation
The ~DOC~ component shows a component's documentation.

For instance:

~** DOC EXAMPLE~

****** DOC EXAMPLE
****** TODO list components
~DOC LIST!~ should list available components.
I just finished a refactor that makes this possible.

**** Routes
Interactive-cv integrates a [[https://github.com/metosin/reitit][reitit]] frontend router.
- Add a [[https://orgmode.org/guide/Tags.html][tag]] to any heading to declare its route.
  - For instance, ~** example :example:~

- A heading's url-path corresponds to its tag.
  - For instance, ~[[example][link to the example page]]~
  - [[example][link to the example page]]

***** TODO Internal org-mode links
Currently, internal links to headlines don't work as they should.
You have to explicitly add the route name.

For instance, ~[[example][This example link]]~ works, but ~[[example]]~ doesn't.


**** TODO Include media
This works using [[https://orgmode.org/manual/Adding-Hyperlink-Types.html][org link types]].
I think it could use more polish, though.

For instance:
#+begin_src org
[[file:img/cemn1.jpg]]
[[youtube:QvNpeMoDJLs]]
[[embed:https://example.com]]
#+end_src

[[file:img/cemn1.jpg]]
[[youtube:QvNpeMoDJLs]]
[[embed:https://example.com]]

*** Build your site :build:
- Shadow-cljs uses [[https://www.gnu.org/software/emacs/][emacs]] to parse your ~content.org~ file.
- It emits a ~content.edn~ file.
- Interactive-cv uses [[https://reagent-project.github.io/][reagent]] to render your site from ~content.edn~.
  
**** Build environment
***** TODO Shadow-cljs

***** TODO Kimacs
- kimacs-project-cv-build ::
- kimacs-project-cv-content-branch ::
- dir-locals shadow default ::

*** Iterate your design :design:
**** TODO Implement an element
Apart from components, interactive-cv represents [[https://orgmode.org/worg/org-syntax.html][org syntax]] with elements.

**** TODO Implement a component
Interactive-cv implements each [[add-content#Components][component]] with [[https://reagent-project.github.io/][reagent]].

- For instance, [[https://gitlab.com/kimok/interactive-cv/-/blob/demo/src/interactive_cv/demo/view.cljs][src/interactive_cv/demo/view.cljs]] implements ~EXAMPLE~:

#+begin_src clojure :file
(defmethod v/component ::EXAMPLE [_]
  [:div {:style {:display    "inline-block" 
                 :background (str "linear-gradient("
                                  "rgb(128, 255, 0), "
                                  "rgb(0, 255, 255))")}}])
#+end_src

**** Style

Most elements & components declare their style with [[https://reagent-project.github.io/][reagent-hiccup]].

You can also use [[https://en.wikipedia.org/wiki/CSS][plain css]]:
- Declare rules in [[https://gitlab.com/kimok/interactive-cv/blob/demo/public/css/style.css][style.css]].
- Include stylesheets in [[https://gitlab.com/kimok/interactive-cv/blob/demo/public/index.html][index.html]].

***** TODO use [[https://github.com/thheller/shadow-css][shadow-css]].

I think this css library would integrate cleanly with the component system.
**** TODO org properties
The ~ashaw~ branch prototypes a system of document- and component-local variables.

I'm still polishing it.
**** TODO radio links
I hope to build a UX for cross-referencing radio-linked terms.
*** Deploy your site :deploy:
**** TODO Gitlab CI
~.gitlab.ci.yml~ declares a simple build pipeline for interative-cv.

Combined with a properly configured repo, it deploys to the web.

To deploy this demo site, I created:
- A gitlab mirror repository
  - name :: kimok/interactive-cv-demo
  - default branch :: demo
  - deployment target :: Gitlab Pages
  - CI/CD variables
    - $KIMOK_CV_SHADOW_BUILD :: demo
      - Protect :: false

**** TODO Kimacs
[[https://gitlab.com/kimok/kimacs][kimacs]] has shortcuts for publishing.

I'm still polishing them.

- ~<escape> p P~ :: ~kimacs-project-cv-publish~

** example :example:
This is an example page.

- Its heading is ~*** example :example:~
- Because it's tagged, interactive-cv considers it a distinct page and renders it here.
- It's a subheading of ~** Interactive CV :interactive_cv:~,
  but interactive-cv doesn't render it on the [[interactive_cv][Interactive CV]] page.

#+begin_src
 ** example :example:
 This is an example page.

 - Its heading is ~*** example :example:~
 - Because it's tagged, interactive-cv considers it a distinct page and renders it here.
 - It's a subheading of ~** Interactive CV :interactive_cv:~,
   but interactive-cv doesn't render it on the [[interactive_cv][Interactive CV]] page.
#+end_src

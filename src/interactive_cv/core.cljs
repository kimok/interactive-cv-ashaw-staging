(ns interactive-cv.core
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require
   [interactive-cv.pdf :as pdf]
   [interactive-cv.zip :as cv.zip]
   [interactive-cv.org-edn :as org]
   [interactive-cv.route :as route :refer [route]]
   [interactive-cv.util :as u]
   [cljs.core.async :refer [<! timeout go-loop]]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [clojure.zip :as z]
   [cljs-http.client :as http]
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [reagent.ratom :refer [reaction]]
   [goog.object :as gobj]
   [gnu.org-mode :as-alias org-mode]
   shadow.resource))

(def content (r/atom nil))

(def zipper
  (reaction
   (some-> (org/zipper @content)
           (cv.zip/seek
            (cond->> org/route @route (comp #{(route/unslug @route)}))))))

(def node (reaction (some-> @zipper z/node)))

(def level
  (reaction
   (some-> @node (org/prop :level))))

(def root-route
  (reaction
   (some-> @content
           org/zipper
           (cv.zip/seek org/route)
           z/node
           org/route)))

(def page
  (reaction
   (some-> @content
           org/zipper
           (cv.zip/seek
            (cond->> org/route
              @route (comp #{(route/unslug @route)})))
           z/node
           org/zipper
           z/next
           (cv.zip/walk cv.zip/drop org/route))))

(def child-routes (reaction (some->> @zipper z/children (filter org/route))))

(def parent-route (reaction (some-> @zipper z/up z/node)))

(def next-route (reaction (some-> @zipper
                                  z/next
                                  (cv.zip/seek org/route)
                                  z/node
                                  org/route)))

(def prev-route (reaction
                 (some-> @zipper
                         z/prev
                         (cv.zip/transduce z/prev cv.zip/root? org/route)
                         z/node
                         org/route)))

(def org-header
  (reaction (some-> (org/zipper @content)
                    (cv.zip/seek #(#{::org-mode/keyword} (org/node-type %)))
                    z/up
                    z/node
                    org/children->kv
                    (u/map-keys #(some-> % name str/lower-case keyword)))))

(def defaults
  {:heading-font "serif"
   :transition-time "0s"
   :opacity 0
   :once nil
   :shadow-x 1
   :shadow-y 1})

(def settings
  (reaction (merge defaults @org-header)))

(def drawer
  (reaction (some-> @zipper org/drawer z/node org/children->kv)))

(defonce state (r/atom {:once nil
                        :opacity 0}))

(def context
  (reaction (merge @org-header @drawer @state)))

(defonce once
  (go (<! (timeout 100))
      (swap! state merge {:once true})))

(defonce tick (go-loop []
                (swap! state update :seconds inc)
                (<! (timeout 1000))
                (recur)))

(when-let [^js pdfjs (gobj/get js/window "pdfjs-dist/build/pdf")]
  (set! (.. pdfjs -GlobalWorkerOptions -workerSrc)
        "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.8.335/pdf.worker.min.js")
  (pdf/init))

(defn fetch-content [& [{:keys [on-response]}]]
  (go (let [response (<! (http/get "content.edn"))
            body (:body response)]
        (reset! content (cond-> body (string? body) edn/read-string))
        (when on-response (on-response))
        (when-not @route (route/go! @root-route)))))

(defn mount [component]
  (when-let [el (.getElementById js/document "app")]
    (rdom/render [component] el)))

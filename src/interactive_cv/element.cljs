(ns interactive-cv.element
  (:require [clojure.edn :as edn]
            [clojure.zip :as z]
            [interactive-cv.org-edn :as org :refer [element]]
            [interactive-cv.util :as u]
            [interactive-cv.core :as cv]
            [interactive-cv.defer :as defer]
            [interactive-cv.pdf :as pdf]
            [gnu.org-mode :as-alias org-mode]
            [gnu.org-mode.link :as-alias link]
            [gnu.org-mode.link.file :as-alias file])
  #_(:require-macros [interactive-cv.element :refer [defel]]))

(defmethod element ::org-mode/italic [_] [:i])

(defmethod element ::org-mode/bold [_] [:strong])

(defmethod element ::org-mode/underline [_] [:u])

(derive ::org-mode/verbatim ::org-mode/code)

(defmethod element ::org-mode/code [[_ {:keys [value]}]]
  [:code value])

(defmethod element ::org-mode/strike-through [[_ {:keys [value]}]]
  [:span {:style {:text-decoration "line-through"}} value])

(defmethod element ::org-mode/subscript [_] [:<> "_"])

(defmethod element ::org-mode/property-drawer [_] [:<>])

(defmethod element ::org-mode/node-property [_] [:<>])

(defmethod element ::org-mode/subscript [_] [:<> "_"])

(defmethod element ::org-mode/headline [_] [:<>])

(defmethod element ::org-mode/title [node]
  [(keyword (str "h" (- (inc (org/prop node :level)) @cv/level)))])

(defmethod element ::org-mode/link.file [node]
  (let [res (-> node (org/with-type
                       (keyword (namespace ::org-mode/_)
                                (str "file."
                                     (-> node (org/prop :path) u/file-extension)))))]
    [:<> res]))

(doall (for [t u/image-file-types
             :let [k (keyword (namespace ::org-mode/_) (str "file." (name t)))]]
         (derive k ::org-mode/file.image)))

(defmethod element ::org-mode/file.image [node]
  [defer/placeholder
   [:img {:style {:max-width "100%"}
          :src (org/prop node :path)}]])

(defmethod element ::org-mode/file.pdf [[_ {:keys [path]}]]
  [defer/placeholder
   (into [:div]
         (for [n (rest (range 4))]
           [:f> pdf/pdf-canvas {:url         path
                                :page-number n}]))])

(derive ::org-mode/link.mailto ::org-mode/link.http)
(derive ::org-mode/link.https ::org-mode/link.http)

(defmethod element ::org-mode/link.http [s]
  [:a {:href (org/prop s :raw-link)}])

(derive ::org-mode/link.gitlab ::org-mode/link.git)
(derive ::org-mode/link.github ::org-mode/link.git)

(derive ::org-mode/link.embed:https ::org-mode/link.embed)

(defmethod element ::org-mode/link.embed [[_ {:keys [path]}]]
  [:iframe {:src path
            :loading "lazy"
            :style {:width "100%" :height 600}}])

(defmethod element ::org-mode/link.youtube [[_ {:keys [path]}]]
  [:iframe {:src (str "https://www.youtube.com/embed/" path)
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/link.vimeo [[_ {:keys [path]}]]
  [:iframe {:src (str "https://player.vimeo.com/video/"
                      path
                      "?title=0&byline=0&portrait=0&app_id=122963")
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/link.soundcloud [[_ {:keys [path]}]]
  [:iframe {:src (str "https://w.soundcloud.com/player/"
                      "?url=https%3A//api.soundcloud.com/playlists/"
                      path
                      "&color=%23ff5500&auto_play=false&hide_related=false"
                      "&show_comments=true&show_user=true&show_reposts=false"
                      "&show_teaser=true&visual=true")
            :loading "lazy"
            :allow-full-screen true
            :style {:width 700 :height 400}}])

(defmethod element ::org-mode/plain-list.ordered [_] [:ol])

(defmethod element ::org-mode/plain-list.unordered [_] [:ul])

(defmethod element ::org-mode/horizontal-rule [_] [:hr])

(defmethod element ::org-mode/plain-list.descriptive [_] [:ul])

(derive ::org-mode/plain-list.ordered ::org-mode/list)
(derive ::org-mode/plain-list.unordered ::org-mode/list)
(derive ::org-mode/plain-list.descriptive ::org-mode/list)

(defmethod element ::org-mode/item [node]
  (let [[item lists] (->> (org/children node)
                          (partition-by
                           #(isa? (org/node-type %)
                                  ::org-mode/list)))]
    (-> ^:drop-children
     [:li]
        (conj
         (into [:div {:style {:display "flex"}}]
               item))
        (into lists))))

(defmethod element ::org-mode/item.description [_] [:p])

(defmethod element ::org-mode/item.bullet [node]
  [:label {:style {:margin-right ".25rem"}} (org/prop node :bullet)])

(defmethod element ::org-mode/tag [node]
  (-> (org/loc node)
      (z/insert-right [::item.separator])
      (z/edit org/with-type :strong)))

(defmethod element ::item.separator [_]
  [:label {:style {:margin "0 .15rem 0 .15rem"}} "◦"])

(defmethod element ::org-mode/table.org [_]
  [:div {:style {:width "100%"}}])

(defmethod element ::org-mode/table-row.standard [_]
  [:div {:style {:display "flex"
                 :width "100%"
                 :justify-content "space-around"}}])

(defmethod element ::org-mode/table-cell [[_ {:keys [value]}]]
  [:div (str value)])

(defmethod element ::org-mode/span [_] [:span])

(defmethod element ::org-mode/src-block [[_ {:keys [value]}]]
  [:pre {:style {:background "lightgray"}} value])

(defmethod element ::org-mode/quote-block [_ {:keys [value]}]
  [:div {:style {:text-align    "justify"
                 :background "white"
                 :margin-bottom "2rem"}}
   value])

(defmethod element ::org-mode/special-block.citation [[_ {:keys [parameters]} :as node]]
  (let [loc (org/loc node)
        {:keys [author]} (edn/read-string
                          (str "{" parameters "}"))]
    (-> loc
        (z/replace
         (z/make-node
          loc
          [:div {:style {:text-align    "justify"
                         :background "white"
                         :margin-bottom "2rem"}}]
          (z/children loc)))
        (z/append-child [:strong {:style {:margin-left "1rem"}} "- " (str author)]))))

(defmethod element ::org-mode/special-block.poem [node]
  ^:drop-children
  [:pre {:style {:text-align    "justify"
                 :margin-bottom "2rem"
                 :padding "0 2rem 0 2rem"
                 :font-family "inherit"}}
   (org/prop node :value)])
